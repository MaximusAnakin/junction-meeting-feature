# Junction App meeting feature
This is a coding assignment created for Junction app - a platform that lets anyone start their own hackathon. This meeting feature allows a user to scroll through participating teams and apply to their available roles. Project is created with a MERN stack (MondoDB, Express, React and Node). 

## Features
- Homepage gets and shows all teams from database.
- User can filter teams based on roles and challenges.
- Team cards show team info. 
- Clicking on roles in cards opens application view where user can write their motivation letter. Component sends application data including users details (dummy data) to the database. 
- Header has links to homepage and applications page. 
- Applications page currently gets all applications from backend instead of ones for particular team. Approve and Decline buttons are placeholders.

## Running the project locally
```
- To run the project locally, you need to have Node.js installed.
- Download project folder
- Create .env file in the root of backend folder and include a "DATABASE_URL=<connection_string>" with MongoDB connection string. 
- Open backend folder in terminal and run "npm i". Then run "npm start". Do the same in frontend folder.
- Go to http://localhost:3000/ in your browser to view the project. 
- To add teams to the database you can use Postman. You can also use Rest Client plugin in VSCode and create teams with file in backend > routes > restClient > applicationRoutes.rest.
```

## Creator
- Maxim Anikin     https://gitlab.com/MaximusAnakin