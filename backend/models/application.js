const mongoose = require("mongoose");

const applicationSchema = new mongoose.Schema({
  name: String,
  title: String,
  email: String,
  about: String,
  skills: [String],
  team: {
    type: mongoose.Schema.Types.ObjectID,
    ref: "Team",
  },
  message: String,
});

module.exports = mongoose.model("Application", applicationSchema);
