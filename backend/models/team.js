const mongoose = require("mongoose");

const teamSchema = new mongoose.Schema({
  title: String,
  challenge: String,
  idea: String,
  roles: [String],
  members: [String],
  email: String,
  description: String,
});

module.exports = mongoose.model("Team", teamSchema);
