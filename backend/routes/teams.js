const express = require("express");
const Team = require("../models/team");
const router = express.Router();

// GET /teams
router.get("/", async (req, res) => {
    try {
      const teams = await Team.find();
      res.json(teams);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  });
  
  // GET /teams/:id
  router.get("/:id", async (req, res) => {
    try {
      const team = await Team.findById(req.params.id);
      if (team == null) {
        return res.status(404).json({ message: "Team not found" });
      }
      res.json(team);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  });
  
  // POST /teams
  router.post("/", async (req, res) => {
    const team = new Team({
      title: req.body.title,
      challenge: req.body.challenge,
      idea: req.body.idea,
      roles: req.body.roles,
      members: req.body.members,
      email: req.body.email,
      description: req.body.description
    });
  
    try {
      const newTeam = await team.save();
      res.status(201).json(newTeam);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  });

  // PATCH /teams/:id
router.patch("/:id", async (req, res) => {
  try {
    const team = await Team.findById(req.params.id);
    if (!team) {
      return res.status(404).json({ message: "Team not found" });
    }

    // Update the team object with the new data from the request body
    if (req.body.title) {
      team.title = req.body.title;
    }
    if (req.body.challenge) {
      team.challenge = req.body.challenge;
    }
    if (req.body.idea) {
      team.idea = req.body.idea;
    }
    if (req.body.roles) {
      team.roles = req.body.roles;
    }
    if (req.body.members) {
      team.members = req.body.members;
    }
    if (req.body.email) {
      team.email = req.body.email;
    }
    if (req.body.description) {
      team.description = req.body.description;
    }

    const updatedTeam = await team.save();
    res.json(updatedTeam);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

  

module.exports = router;
