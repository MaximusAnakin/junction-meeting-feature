require('dotenv').config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const app = express();

mongoose.connect(process.env.DATABASE_URL);
const db = mongoose.connection;
db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Connected to Database"));

app.use(express.json());
app.use(cors());

const teamsRouter = require('./routes/teams');
app.use("/teams", teamsRouter);

const applicationsRouter = require('./routes/applications');
app.use("/applications", applicationsRouter);

const port = 3001;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
