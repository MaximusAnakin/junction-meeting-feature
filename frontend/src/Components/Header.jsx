import { Stack, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <Stack
      direction="row"
      alignItems="start"
      justifyContent="space-between"
      p="10px"
    >
      <Typography variant="h6">Junction App Meeting Feature</Typography>
      <div>
        <Link to="/applications">
          <button>Applications</button>
        </Link>

        <Link to="/">
          <button>Teams to join</button>
        </Link>
      </div>
    </Stack>
  );
};

export default Header;
