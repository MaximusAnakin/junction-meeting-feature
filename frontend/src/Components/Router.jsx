import React from 'react'
import { Routes, Route } from "react-router-dom";
import Join from './Join';
import Apply from './Apply';
import Applications from './applications/Applications';

const Router = () => {
  return (
    <Routes>
		<Route path="/" element={<Join />} />
		<Route path="/apply" element={<Apply />} />
		<Route path='/applications' element={<Applications />} />
		<Route path="*" element={<>Page not found</>} />
	</Routes>
  )
}

export default Router