import * as React from "react";
import {
  styled,
  Card,
  CardHeader,
  CardContent,
  Stack,
  Box,
} from "@mui/material";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Link } from "react-router-dom";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function TeamCard({
  name,
  hashtag,
  description,
  idea,
  roles,
  members,
  email,
  teamId
}) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 400, bgcolor: "#f9f9f9", marginBottom: "20px" }}>
      <CardHeader
        action={<IconButton sx={{ fontSize: "20px" }}>{"#" + hashtag}</IconButton>}
        title={name}
        titleTypographyProps={{ fontSize: "20px" }}
      />
      <CardContent>
        <Typography paragraph variant="body2" color="text.secondary">
          {idea}
        </Typography>
        <Typography paragraph variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Typography variant="h5">We are looking for</Typography>
        {/* Available roles */}
        <Stack direction="row" justifyContent="center">
          {roles.map((role, index) => (
            <Stack
              key={index}
              component={Link}
              to={`/apply?team=${name}&challenge=${hashtag}&role=${role}&teamId=${teamId}`}
              sx={{
                borderRadius: "15px",
                bgcolor: "#82c8ef",
                justifyContent: "center",
                alignItems: "center",
                m: 1,
                height: "60px",
                width: "100px",
                textDecoration: "none"
              }}
            >
              <Typography>{role}</Typography>
            </Stack>
          ))}
        </Stack>
      </CardContent>
      <CardActions disableSpacing>
        <ExpandMore expand={expanded} onClick={handleExpandClick}>
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography variant="h5">Who we are</Typography>
          {/* Team members */}
          <Stack direction="row" justifyContent="center">
            {members.map((member, index) => (
              <Stack alignItems="center" key={index}>
                <Box
                  sx={{
                    border: "solid #A3F7CB 2px",
                    bgcolor: "#DADADA",
                    borderRadius: "15px",
                    m: 1,
                    height: "120px",
                    width: "100px",
                  }}
                ></Box>
                <Typography>{member}</Typography>
              </Stack>
            ))}
          </Stack>

          <Stack alignItems="center" marginTop="20px">
            <Typography>Get in touch</Typography>
            <Typography variant="h6"></Typography>
            <a href={`mailto:${email}`}>{email}</a>
          </Stack>
        </CardContent>
      </Collapse>
    </Card>
  );
}
