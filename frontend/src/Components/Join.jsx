import React, { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import TeamCard from "./TeamCard";
import { Stack } from "@mui/material";
import axios from "axios";
import SearchField from "./SearchField";

const Join = () => {
  const [teamData, setTeamData] = useState(null);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:3001/teams")
      .then((response) => {
        setTeamData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const filteredData = teamData
  ? teamData.filter((team) => {
      const challengeMatch = team.challenge.toLowerCase().includes(searchValue.toLowerCase());
      const rolesMatch = team.roles.some(role => role.toLowerCase().includes(searchValue.toLowerCase()));
      return challengeMatch || rolesMatch;
    })
  : [];

  return (
    <Stack sx={{ p: "0px 10px"}}>
      <Stack direction="row" alignItems="center" justifyContent="space-between">
        <Typography variant="h3">Join a team</Typography>
        <SearchField
          searchValue={searchValue}
          setSearchValue={setSearchValue}
        />
      </Stack>

      <Typography color="text.secondary">
        Apply for a role that suits you the most and join an existing team.
      </Typography>

      <Stack
        direction="row"
        flexWrap="wrap"
        justifyContent="space-around"
        alignItems="baseline"
        marginTop="20px"
      >
        {filteredData.map((team) => (
          <TeamCard
            key={team._id}
            teamId={team._id}
            name={team.title}
            hashtag={team.challenge}
            description={team.description}
            idea={team.idea}
            roles={team.roles}
            members={team.members}
            email={team.email}
          />
        ))}
      </Stack>
    </Stack>
  );
};

export default Join;
