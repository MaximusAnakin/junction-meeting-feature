import React from 'react';

const SearchField = ({ searchValue, setSearchValue }) => {
  return (
    <div>
      <input
        type="text"
        value={searchValue}
        onChange={(e) => setSearchValue(e.target.value)}
        placeholder="Search"
      />
    </div>
  );
};

export default SearchField;
