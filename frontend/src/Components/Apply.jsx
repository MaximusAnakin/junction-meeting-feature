import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Stack, TextField } from "@mui/material";
import axios from "axios";

const Apply = () => {
  const [inputValue, setInputValue] = useState("");
  const [applicationSent, setApplicationSent] = useState(false);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  //This gets props from the url of the page that get passed from TeamCard when clicking a role.
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const team = queryParams.get("team");
  const challenge = "#" + queryParams.get("challenge");
  const role = queryParams.get("role");
  const teamId = queryParams.get("teamId");

  //dummy userinfo which should come from users account
  const user = {
    name: "Alex Rodriguez",
    title: "Backend Developer",
    email: "alex.rodriguez@example.com",
    about: "I am a backend developer with 5 years of experience",
    skills: ["Node.js", "Express", "MongoDB"],
  };

  //application data to be sent
  const applicationData = {
    name: user.name,
    title: user.title,
    email: user.email,
    about: user.about,
    skills: user.skills,
    team: teamId,
    message: inputValue,
  };

  //sends application for chosen position at a chosen team.
  const handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:3001/applications", applicationData)
      .then((response) => {
        console.log("Application created successfully!");
        console.log(response.data);
        setApplicationSent(true);
      })
      .catch((error) => {
        console.log("Error creating application:");
        console.error(error);
      });
  };

  const buttonStyle = {
    backgroundColor: "#A3F7CB",
    color: "#000",
    borderRadius: "10px",
    width: "150px",
    marginBottom: 10,
    marginLeft: 10,
  };

  return (
    <>
      <Stack sx={{ p: 5 }}>
        <Typography variant="h3">Apply</Typography>
        <Typography color="text.secondary">
          Send your profile information and motivation message to {team}.
        </Typography>

        <Card
          sx={{
            width: "100%",
            alignSelf: "center",
            m: 5,
            backgroundColor: "#F9F9F9",
            height: "300px",
          }}
        >
          {applicationSent ? (
            <Stack
              sx={{
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Typography variant="h6" margin="20px">Form sent successfully!</Typography>
              <Typography>
                Please wait for the team’s response in your mail.
              </Typography>
            </Stack>
          ) : (
            <form onSubmit={handleSubmit}>
              <CardContent>
                <Typography variant="h4" color="text.primary">
                  {team}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="#A3F7CB">
                  {challenge}
                </Typography>

                <Typography variant="body2" gutterBottom>
                  Why do you want to apply for the position of a {role} and work
                  with “{team}?”
                  <br />
                  Max. 400 words
                </Typography>
                <TextField
                  label="Enter your text here"
                  variant="outlined"
                  value={inputValue}
                  onChange={handleInputChange}
                  sx={{
                    width: "100%",
                    marginTop: "10px",
                    backgroundColor: "white",
                  }}
                />
              </CardContent>
              <CardActions>
                <Button variant="contained" type="submit" style={buttonStyle}>
                  Send
                </Button>
              </CardActions>
            </form>
          )}
        </Card>
      </Stack>
    </>
  );
};

export default Apply;
