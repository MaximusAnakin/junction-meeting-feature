import axios from 'axios';
import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import Application from './Application';
import { Stack } from '@mui/system';

const Applications = () => {
    const [applications, setApplications] = useState([]);

    //gets all applications
    useEffect(() => {
      axios
        .get("http://localhost:3001/applications")
        .then((response) => {
          setApplications(response.data);
        })
        .catch((error) => {
          console.error(error);
        });
    }, []);

  return (
    <div style={{margin: "20px"}}>
      <h1>Pending applications</h1>
      <Stack direction="row" gap={5} flexWrap="wrap">
        {applications.map((application) => (
            <Application 
            key={application._id}
            name={application.name}
            title={application.title}
            email={application.email}
            about={application.about}
            skills={application.skills}
            message={application.message}
            />
        ))}
      </Stack>
    </div>
  )
}

export default Applications