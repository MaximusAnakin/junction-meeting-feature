import * as React from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Stack } from "@mui/material";

const Application = ({ name, skills, title, message }) => {

    const buttonStyle = {
        color: "#000",
        borderRadius: "10px",
        width: "150px",
        marginBottom: 10,
        marginLeft: 10,
      };

  return (
    <Stack sx={{ width: 500, height: 300, backgroundColor: "#F9F9F9", p: 2}} justifyContent="space-between">
        <Stack direction="row">
          <Typography sx={{ fontSize: 25 }} color="#94DDB7" gutterBottom>
            {name}
          </Typography>
          <Typography
            sx={{ fontSize: 25, mb: 1.5, ml: 1 }}
            color="text.secondary"
          >
            {"/" + title}
          </Typography>
        </Stack>
        <Stack>
          <Typography marginBottom="20px">{message}</Typography>
          <>
            {skills.map((skill) => (
              <Typography>{skill}</Typography>
            ))}
          </>
        </Stack>
    
        <Stack direction="row" justifyContent="flex-end" alignItems="end">
          <Button variant="contained" style={buttonStyle} sx={{backgroundColor: "#A3F7CB"}}>Approve</Button>
          <Button variant="contained" style={buttonStyle} sx={{backgroundColor: "#FCE0D8"}}>Decline</Button>
        </Stack>
      
    </Stack>
  );
};

export default Application;
